<?php

/**
 * Config page form for "pin my map".
 */
function pin_my_map_form_config($form, &$form_state) {

  $query = db_select('pin_my_map', 'm');
  $results = $query
        ->fields('m', array('latitude', 'longitude', 'zoom', 'scroll', 'poi', 'clickZoom'))
        ->execute();

  foreach($results as $result){
    $latitude = $result->latitude;
    $longitude = $result->longitude;
    $zoom = $result->zoom;
    $scroll = $result->scroll;
    $poi = $result->poi;
    $clickZoom = $result->clickZoom;
  }

  $form['latitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Latitude'),
    '#default_value' => $latitude,
    '#required' => TRUE,
    '#element_validate' => array('pin_my_map_lat_validate'),
  );

  $form['longitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Longitude'),
    '#default_value' => $longitude,
    '#required' => TRUE,
    '#element_validate' => array('pin_my_map_lng_validate'),
  );  

  $form['zoom'] = array(
    '#type' => 'select',
    '#title' => t('Zoom'),
    '#options' => array(
      0 => 6,
      1 => 8,
      2 => 10,
      3 => 12,
      4 => 14,
      5 => 16,
      6 => 18,
      7 => 20,
      8 => 22,
    ),
    '#default_value' => $zoom,
  );  

  $form['scroll'] = array(
    '#type' => 'select',
    '#title' => t('Scroll'),
    '#options' => array(
      0 => t('False'),
      1 => t('True'),
    ),
    '#default_value' => $scroll,
  );   

  $form['poi'] = array(
    '#type' => 'select',
    '#title' => t('Points of Interest'),
    '#options' => array(
      0 => t('On'),
      1 => t('Off'),
    ),
    '#default_value' => $poi,
  );  

  $form['clickZoom'] = array(
    '#type' => 'select',
    '#title' => t('Disable Double Click Zoom'),
    '#options' => array(
      0 => t('False'),
      1 => t('True'),
    ),
    '#default_value' => $clickZoom,
  );  

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );

  return $form;
}

function pin_my_map_lat_validate($element, &$form_state, $form) {
  $value = $element['#value'];
  if (preg_match("/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/", $value)) {
    return true;
  } else {
    form_error($element, t('Please enter a decimal value.'));
  }
}

function pin_my_map_lng_validate($element, &$form_state, $form) {
  $value = $element['#value'];
  if (preg_match("/^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}$/", $value)) {
    return true;
  } else {
    form_error($element, t('Please enter a decimal value.'));
  }
}

function pin_my_map_form_config_submit($form, &$form_state) {

  $query = db_update('pin_my_map');
  $result = $query
  ->fields(array(
    'mid' => 1,
    'latitude' => $form_state['values']['latitude'],
    'longitude' => $form_state['values']['longitude'],
    'zoom' => $form_state['values']['zoom'],
    'scroll' => $form_state['values']['scroll'],
    'poi' => $form_state['values']['poi'],
    'clickZoom' => $form_state['values']['clickZoom'],
  ))
  ->execute();

  drupal_set_message(t('All settings saved'), 'status', FALSE);

}


