(function ($) {
      $( document ).ready(function() {
      
      var map;
      var lat = Drupal.settings.pin_my_map.lat;
      var lng = Drupal.settings.pin_my_map.lng;
      var latlng = new google.maps.LatLng(lat, lng);
      var scroll = Drupal.settings.pin_my_map.scroll;
      var drag = false;

      // Determine zoom level
      switch(Drupal.settings.pin_my_map.zoom) {
        case '0':
          zoom = 6;
          break;
        case '1':
          zoom = 8;
          break;
        case '2':
          zoom = 10;
          break;
        case '3':
          zoom = 12;
          break;
        case '4':
          zoom = 14;
          break;
        case '5':
          zoom = 16;
          break;
        case '6':
          zoom = 18;
          break;
        case '7':
          zoom = 20;
          break;
        case '8':
          zoom = 22;
          break;
        default:
          zoom = 6;
      }

      // Determine if points of interest is on or off
      switch(Drupal.settings.pin_my_map.poi) {
        case '0':
          poi = "on";
          break;
        case '1':
          poi = "off";
          break;
        default:
          poi = "on";
      }  

      // Determine click zoom
      switch(Drupal.settings.pin_my_map.clickZoom) {
        case '0':
          clickZoom = false;
          break;
        case '1':
          clickZoom = true;
          break;
        default:
          clickZoom = true;
      }   

        var mapOptions = {
          center: latlng,
          zoom: zoom,
          scrollwheel: scroll,
          draggable: false,
          disableDoubleClickZoom: clickZoom,
          navigationControl: false,
          mapTypeControl: false,
          navigationControl: false,
          scaleControl: false,
          panControl: false,    
          disableDefaultUI: true,    
          styles:[
          {
              "featureType": "poi",
              "stylers": [
                  {
                      "visibility": poi
                  }
              ]
          }
          ],
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };      
        
        
        var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            animation: google.maps.Animation.DROP,
        });


        // responsive map
        google.maps.event.addDomListener(window, "resize", function() {
          var center = map.getCenter();
          google.maps.event.trigger(map, "resize");
          map.setCenter(center); 
        });


        $(".onoffswitch-checkbox").click(function(){
          if($(".onoffswitch-checkbox").is(':checked') == false) {              
            toggleDrag('unlock');
          } else {
            toggleDrag('lock');
          }
        });
           
      function toggleDrag(i) {
        if (i === 'lock') { 
          drag = false;
        } else {
          drag = true;
        }
        
        map.setOptions({
          draggable: drag,
        });
      }        

        
  });
})(jQuery);
